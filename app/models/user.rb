class User < ActiveRecord::Base
  before_save { self.email = email.downcase }
  validates :name, presence: true
  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+.[a-z]+\z/i
  validates :email, presence: true, format: { with: VALID_EMAIL_REGEX }, uniqueness: { case_sensitive: false }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
  has_many :articles, dependent: :destroy
  has_many :favourite_posts, dependent: :destroy
  has_many :accounts, dependent: :destroy
  mount_uploader :avatar, AvatarUploader
  paginates_per 10
end
