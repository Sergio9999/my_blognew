class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments
  has_many :favourite_posts
  has_and_belongs_to_many :categories
  validates :user_id, presence: true
  validates :title, presence: true, length: { minimum: 6 }
  mount_uploader :image, ImageUploader
  paginates_per 10
end
