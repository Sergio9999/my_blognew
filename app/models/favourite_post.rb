class FavouritePost < ActiveRecord::Base
  belongs_to :user
  belongs_to :article

  def self.manage(article)
    object = find_by(article: article)
    object ? object.destroy : create(article: article)
  end
end
