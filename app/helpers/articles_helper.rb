module ArticlesHelper
  def favourite_post(article)
    if current_user.favourite_posts.map(&:article).include?(article)
      'text-muted'
    else
      'text-success'
    end
  end
end
