class StaticPagesController < ApplicationController
  def index
    @articles = Article.all.includes(:categories).page(params[:page])
  end
end
