class Admin::ArticlesController < AdminsController
  def index
    @articles = Article.all
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
  end
end
