class UsersController < ApplicationController
  before_action :signin_user, only: [:show, :edit, :update]

  def new
    @user = User.new
  end

  def index
    @users = User.all.page(params[:page])
  end

  def create
    @user = User.new(user_params)
    if current_admin
      @user.update_attributes(user_params, admin: true)
    end
    if @user.save
      UserMailer.signup_confirmation(@user).deliver
      session[:user_id] = @user.id
      redirect_to @user.email, notice: 'Thank you for signed up!!!'
    else
      render 'new'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
  end

  def update
    if current_admin
      @user.update_attributes(user_params)
    end
    if current_user.update_attributes(user_params)
      flash[:success] = 'Profile updated'
      redirect_to current_user
    else
      render'edit'
    end
  end

  private

  def user_params
    params.require(:user).permit(:name, :email, :password, :password_confirmation, :avatar, :admin)
  end

  def signin_user
    @user = User.find(params[:id])
    redirect_to signin_path unless current_user == @user
  end
end
