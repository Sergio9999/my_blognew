class OmniauthController < ApplicationController
  def create
    @account = Account.find_by(email: auth_hash[:info][:email])
    if @account
    else
      @user = User.find_by(email: auth_hash[:info][:email])
      if @user
        account_create
      else
        @user = User.new(vk_params)
        account_create
        @account.save
        UserMailer.inviters_mailer(@user).deliver_now
        flash[:success] = 'Мы отправили письмо с Вашим паролем на Вашу почту. Спасибо за регистрацию!!!'
      end
    end
    session[:user_id] = @account.user_id
    redirect_to current_user
  end

  def account_create
    @account = Account.create(uid: auth_hash[:uid],
                              provider: auth_hash[:provider],
                              email: auth_hash[:info][:email],
                              user: @user)
  end

  private

  def vk_params
    password = "#{rand(100)}_sample_password_#{rand(100)}"
    { name: auth_hash[:info][:name],
      email: auth_hash[:info][:email],
      remote_avatar_url: auth_hash[:extra][:raw_info][:photo_200],
      password: password,
      password_confirmation: password
    }
  end

  def auth_hash
    request.env['omniauth.auth']
  end
end
