class ArticlesController < ApplicationController
  before_action :authorize, only: [:index, :edit, :update]
  before_action :current_post, only: [:show, :edit, :update, :destroy, :favourite]
  before_action :require_user, only: [:edit, :update, :destroy]

  def index
    @articles = params[:current] ? current_user.articles.includes(:categories).page(params[:page]) : Article.all.includes(:categories).page(params[:page])
  end

  def show
    @article.increment!(:page_views)
  end

  def new
    @article = Article.new
  end

  def create
    @article = current_user.articles.build(article_params)
    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  def edit
  end

  def update
    if @article.update(article_params)
      redirect_to @article
    else
      render 'edit'
    end
  end

  def destroy
    @article.destroy
    redirect_to @article
  end

  def favourite
    current_user.favourite_posts.manage(@article)
  end

  private

  def current_post
    @article = Article.find(params[:id])
  end

  def article_params
    params.require(:article).permit(:title, :text, :integer, :image, category_ids: [])
  end

  def require_user
    redirect_to root_path unless @article.user == current_user
  end
end
