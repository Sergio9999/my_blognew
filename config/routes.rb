Rails.application.routes.draw do
  root 'static_pages#index'
  get :'signin', to: 'sessions#new'
  delete :'signout', to: 'sessions#destroy'
  resources :users
  resources :sessions
  resources :articles do
    get :favourite, on: :member
    resources :comments
  end
  get 'auth/vkontakte/callback', to: 'omniauth#create'
  namespace :admin do
    root 'users#index'
    resources :users, only: [:index, :destroy]
    resources :articles, only: [:index, :destroy]
  end
end
