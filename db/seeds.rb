# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
admin = User.create(name:                  'Sergio9999',
                    email:                 'SobolevSergey9999@gmail.com',
	                  password:              'DR260733',
	                  password_confirmation: 'DR260733'
	                 )

admin.toggle!(:admin)
    if admin.valid?
       admin.save

    elsif admin.errors.any?
      admin.errors.full_messages.each do |msg|
        puts msg
      end
    else
      puts "****NOT VALID****"
    end