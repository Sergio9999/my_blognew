class AddPageViewsToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :page_views, :integer, default: 0
  end
end
